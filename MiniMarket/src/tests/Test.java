package tests;

import model.Product;
import dao.DataAccessImp;
import dao.VirtualDataBase;

public class Test {

	public Test() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		try {
			VirtualDataBase.simulateClient();
			VirtualDataBase.simulateProducts();
			
			System.out.println(VirtualDataBase.products.get(3).getProductQuantity());
			
			Product product=VirtualDataBase.products.get(3);
			DataAccessImp accessImp=new DataAccessImp();
			accessImp.addProductToCart(1, product.getProductId(), 3);
			
			System.out.println(VirtualDataBase.products.get(3).getProductQuantity());
			System.out.println("*******************************************************");
			System.out.println(VirtualDataBase.clients.get(1).getClientCart().getProductsInCart().get(3).getQuantity());
			System.out.println(VirtualDataBase.clients.get(1).getClientCart().getProductsInCart().get(3).getProduct());
			
			accessImp.addProductToCart(1, product.getProductId(), 1);
			System.out.println("*******************************************************");
			System.out.println(VirtualDataBase.clients.get(1).getClientCart().getProductsInCart().get(3).getQuantity());
			System.out.println(VirtualDataBase.clients.get(1).getClientCart().getProductsInCart().get(3).getProduct());

			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
