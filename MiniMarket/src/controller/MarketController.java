package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Client;
import dao.DataAccessImp;

/**
 * Servlet implementation class MarketController
 */
@WebServlet("/MarketController")
public class MarketController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static String SHOW_CART = "/cart.jsp";
    private static String LIST_PRODUCT = "/listProducts.jsp";
    private static String UPDATE_CART = "/updateCart.jsp";
    private static String LOGIN = "/login.jsp";
    private static DataAccessImp accessImp;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MarketController() {
	super();
	accessImp = new DataAccessImp();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {

	String forward = "";
	boolean b = false;
	String action = request.getParameter("action");
	int clientId = -1;
	Object objectIdUser = request.getSession().getAttribute("id_user");
	if (objectIdUser != null) {
	    clientId = (Integer) objectIdUser;
	}

	if (action.equalsIgnoreCase("addToCart")) {
	    if (clientId != -1) {
		int productId = Integer.valueOf(request
			.getParameter("productId"));
		accessImp.addProductToCart(clientId, productId, 1);
		forward = LIST_PRODUCT;

	    } else {
		forward = LOGIN;
	    }

	} else if (action.equalsIgnoreCase("deleteProductFromCart")) {
	    if (clientId != -1) {
		int productId = Integer.valueOf(request
			.getParameter("productId"));
		accessImp.removeProductFromCart(productId, clientId);
		forward = SHOW_CART;
	    } else {
		forward = LOGIN;
	    }

	} else if (action.equalsIgnoreCase("updateCart")) {
	    if (clientId != -1) {
		int productId = Integer.valueOf(request
			.getParameter("productId"));
		try {
		    int qtt = Integer.valueOf(request.getParameter("quantity"));
		    accessImp.updateProductInCart(clientId, productId, qtt);
		    forward = SHOW_CART;
		} catch (Exception e) {
		    b = true;
		    request.setAttribute("error",
			    "Error in update cart: product quantity must be number");
		    RequestDispatcher view = request
			    .getRequestDispatcher("error.jsp");
		    view.forward(request, response);
		}

	    } else {
		forward = LOGIN;
	    }
	} else if (action.equalsIgnoreCase("logout")) {
	    request.getSession().invalidate();
	    forward = LOGIN;
	}
	/*
	 * RequestDispatcher view = request.getRequestDispatcher(forward);
	 * view.forward(request, response);
	 */
	if (!b) {
	    response.sendRedirect(request.getContextPath() + forward);
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {
	String login = request.getParameter("login").trim();
	String password = request.getParameter("password").trim();
	try {
	    Client client = accessImp.login(login, password);
	    if (client != null) {
		request.getSession().setAttribute("user", client.getLogin());
		request.getSession().setAttribute("id_user",
			client.getIdClient());

		response.sendRedirect(request.getContextPath() + LIST_PRODUCT);

	    } else {
		response.sendRedirect(request.getContextPath() + LOGIN);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

}
