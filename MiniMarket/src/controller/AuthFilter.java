package controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Client;
import dao.DataAccessImp;

@WebFilter(filterName = "AuthFilter", urlPatterns = { "*.jsp" })
public class AuthFilter implements Filter {
    private DataAccessImp accessImpl;

    public AuthFilter() {
	accessImpl = new DataAccessImp();
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
	    FilterChain chain) throws IOException, ServletException {
	try {

	    // check whether session variable is set
	    HttpServletRequest req = (HttpServletRequest) request;
	    HttpServletResponse res = (HttpServletResponse) response;
	    HttpSession ses = req.getSession(false);

	    String reqURI = req.getRequestURI();

	    if (reqURI.indexOf("/login.jsp") >= 0

		    || reqURI.indexOf("/index.jsp") >= 0
		    || reqURI.indexOf("/listProducts.jsp") >= 0
		    || reqURI.indexOf("/login.jsp") >= 0
		    || (ses != null && ses.getAttribute("id_user") != null && ((reqURI
			    .indexOf("cart.jsp") >= 0) || (reqURI
			    .indexOf("updateCart.jsp") >= 0))

		    ))
		chain.doFilter(request, response);
	    else

		res.sendRedirect(req.getContextPath() + "/login.jsp");

	} catch (Throwable t) {
	    System.out.println(t.getMessage());
	}
    }

    @Override
    public void destroy() {

    }

    public Boolean validationClt(String user, String Pass) throws Exception {

	Client c = accessImpl.login(user, Pass);
	if (c != null)
	    return true;
	else
	    return false;
    }

}