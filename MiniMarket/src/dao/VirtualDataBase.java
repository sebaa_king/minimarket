package dao;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import model.Client;
import model.Product;

public final class VirtualDataBase {

    public static final Map<Integer, Client> clients = new HashMap<Integer, Client>();
    public static final Map<Integer, Product> products = new LinkedHashMap<Integer, Product>();

    public static final String PRODUCT_NAME_PREFIX = "product";
    public static final String PRODUCT_DESQ_PREFIX = "Product_Desq";
    public static final long PRODUCT_MIN_PRICE = 10;
    public static final long PRODUCT_MAX_PRICE = 30;

    public static final int PRODUCT_MIN_QUANTITY = 1;
    public static final int PRODUCT_MAX_QUANTITY = 10;
    public static final String PRODUCT_IMG = "images/post";

    public static final String CLIENT_LOGIN_PREFIX = "login";
    public static final String CLIENT_PASSWORD_PREFIX = "password"; // hacker
								    // will be
								    // HAPPY :)
    public static final String CLIENT_FIRST_NAME_PREFIX = "first_name";
    public static final String CLIENT_LAST_NAME_PREFIX = "first_name";

    public static void simulateProducts() {
	for (int i = 0; i < 10; i++) {
	    Product product = new Product();
	    product.setProductId(i);
	    product.setProductName(PRODUCT_NAME_PREFIX + i);
	    product.setProductDescription(PRODUCT_DESQ_PREFIX + i);
	    product.setImgProduct(PRODUCT_IMG + i + ".jpg");

	    Random r = new Random();
	    double randomPrice = PRODUCT_MIN_PRICE
		    + (PRODUCT_MAX_PRICE - PRODUCT_MIN_PRICE) * r.nextDouble();
	    randomPrice = (double) Math.round(randomPrice * 100) / 100;
	    product.setProductPrice(randomPrice);
	    int randomQtt = r
		    .nextInt((PRODUCT_MAX_QUANTITY - PRODUCT_MIN_QUANTITY) + 1)
		    + PRODUCT_MIN_QUANTITY;
	    product.setProductQuantity(randomQtt);
	    products.put(i, product);
	}
    }

    public static void simulateClient() throws Exception {
	for (int i = 0; i < 4; i++) {
	    Client client = new Client();
	    client.setLogin(CLIENT_LOGIN_PREFIX + i);
	    client.setIdClient(i);
	    client.setFirstName(CLIENT_FIRST_NAME_PREFIX + i);
	    client.setLastName(CLIENT_LAST_NAME_PREFIX + i);
	    String hash = hashPassword(CLIENT_PASSWORD_PREFIX + i);
	    client.setPassword(hash);
	    clients.put(i, client);
	}
    }

    public static String hashPassword(String pass) throws Exception {
	MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
	messageDigest.update(pass.getBytes("UTF-8"));
	byte[] digest = messageDigest.digest();
	return convertByteArrayToHexString(digest);

    }

    private static String convertByteArrayToHexString(byte[] arrayBytes) {
	StringBuffer stringBuffer = new StringBuffer();
	for (int i = 0; i < arrayBytes.length; i++) {
	    stringBuffer.append(Integer.toString(
		    (arrayBytes[i] & 0xff) + 0x100, 16).substring(1));
	}
	return stringBuffer.toString();
    }
}
