package dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.Cart;
import model.Client;
import model.DetailCart;
import model.Product;

public class DataAccessImp {

    public DataAccessImp() {
	if (VirtualDataBase.products.size() == 0
		|| VirtualDataBase.clients.size() == 0) {
	    try {
		VirtualDataBase.simulateClient();
	    } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    VirtualDataBase.simulateProducts();
	}

    }

    /**
     * Get product list from virtual database
     * 
     * @return list of products
     */
    public List<Product> listProduct() {
	List<Product> products = new ArrayList<Product>();
	Set<Integer> keys = VirtualDataBase.products.keySet();
	for (Integer key : keys) {
	    Product product = VirtualDataBase.products.get(key);
	    products.add(product);
	}
	return products;
    }

    /**
     * get list product detail of client cart
     * 
     * @param clientId
     *            client id
     * @return list of product detail for client cart
     */
    public List<DetailCart> productsInCart(int clientId) {
	List<DetailCart> detailCarts = new ArrayList<DetailCart>();
	Client client = VirtualDataBase.clients.get(clientId);
	if (client != null) {
	    Cart cart = client.getClientCart();
	    Map<Integer, DetailCart> ctr = cart.getProductsInCart();
	    Set<Integer> keys = ctr.keySet();
	    for (Integer key : keys) {
		DetailCart detailCart = ctr.get(key);

		detailCarts.add(detailCart);
	    }

	}
	return detailCarts;
    }

    public Product searchProduct(int productId) {
	return VirtualDataBase.products.get(productId);
    }

    /**
     * calculate total cart price
     * 
     * @param clientId
     *            client id
     * @return total cart price
     */
    public double totalCart(int clientId) {
	List<DetailCart> detailCarts = productsInCart(clientId);
	double totalPrice = 0;
	for (DetailCart detailCart : detailCarts) {
	    Product p = searchProduct(detailCart.getProduct());
	    totalPrice += detailCart.getQuantity() * p.getProductPrice();
	}
	totalPrice = (double) Math.round(totalPrice * 100) / 100;
	return totalPrice;

    }

    /**
     * add product to client cart if not exist or update quantity
     * 
     * @param clientId
     *            client id
     * @param productId
     *            product id
     * @param quantity
     *            quantity to add
     */
    public void addProductToCart(int clientId, int productId, int quantity) {
	Client client = VirtualDataBase.clients.get(clientId);
	if (client != null) {
	    Cart cart = client.getClientCart();
	    Map<Integer, DetailCart> crt = cart.getProductsInCart();

	    Product product = searchProduct(productId);
	    if (product.getProductQuantity() - quantity >= 0) {
		if (crt.containsKey(productId)) {
		    DetailCart detailCart = crt.get(productId);
		    detailCart.setQuantity(detailCart.getQuantity() + quantity);
		} else {
		    DetailCart detailCart = new DetailCart(productId, quantity);
		    crt.put(productId, detailCart);
		}
		int qttInDb = VirtualDataBase.products.get(productId)
			.getProductQuantity();
		VirtualDataBase.products.get(productId).setProductQuantity(
			qttInDb - quantity);
	    }
	}
    }

    /**
     * remove product from client cart
     * 
     * @param productId
     *            product id
     * @param clientId
     *            client id
     */
    public void removeProductFromCart(int productId, int clientId) {
	Client client = VirtualDataBase.clients.get(clientId);
	if (client != null) {
	    Cart cart = client.getClientCart();
	    Map<Integer, DetailCart> crt = cart.getProductsInCart();

	    if (crt.containsKey(productId)) {
		int qtt = crt.get(productId).getQuantity();
		crt.remove(productId);
		if (VirtualDataBase.products.get(productId) != null) {
		    int currentQtt = VirtualDataBase.products.get(productId)
			    .getProductQuantity();
		    VirtualDataBase.products.get(productId).setProductQuantity(
			    currentQtt + qtt);
		}
	    }
	}
    }

    /**
     * delete all product from client cart
     * 
     * @param clientId
     *            client id
     */
    public void trushCart(int clientId) {
	Client client = VirtualDataBase.clients.get(clientId);
	if (client != null) {
	    client.getClientCart().getProductsInCart().clear();
	}

    }

    /**
     * update product quantity in cart
     * 
     * @param clientId
     *            client id
     * @param productId
     *            product id
     * @param quantity
     *            new product quantity
     */
    public void updateProductInCart(int clientId, int productId, int quantity) {
	Client client = VirtualDataBase.clients.get(clientId);
	if (client != null) {
	    Cart cart = client.getClientCart();
	    Map<Integer, DetailCart> crts = cart.getProductsInCart();

	    if (crts.containsKey(productId)) {
		DetailCart detailCart = crts.get(productId);
		Product product = searchProduct(productId);
		int tmp = quantity - detailCart.getQuantity();
		if (product.getProductQuantity() - tmp >= 0) {
		    detailCart.setQuantity(quantity);
		    int qttInDb = VirtualDataBase.products.get(productId)
			    .getProductQuantity();
		    VirtualDataBase.products.get(productId).setProductQuantity(
			    qttInDb - tmp);
		}
	    }
	}

    }

    public int quantityInCart(int clientId, int productId) {
	int qtt = -1;
	Client client = VirtualDataBase.clients.get(clientId);
	if (client != null) {
	    Cart cart = client.getClientCart();
	    Map<Integer, DetailCart> crt = cart.getProductsInCart();

	    if (crt.containsKey(productId)) {
		qtt = crt.get(productId).getQuantity();
	    }
	}
	return qtt;
    }

    public Client SearchClient(String login) {
	Client client = null;
	Set<Integer> keys = VirtualDataBase.clients.keySet();
	for (Integer key : keys) {
	    Client clientDB = VirtualDataBase.clients.get(key);
	    if (clientDB.getLogin().equals(login)) {
		client = clientDB;
	    }
	}
	return client;
    }

    public Client login(String user, String password) throws Exception {
	Client personne = SearchClient(user);
	boolean trouv = false;
	if (personne != null) {
	    String hash = personne.getPassword();
	    String CurrentHash = VirtualDataBase.hashPassword(password);
	    if (hash.equals(CurrentHash)) {
		trouv = true;
	    }
	}
	if (trouv)
	    return personne;
	else
	    return null;
    }

}
