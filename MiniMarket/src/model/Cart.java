package model;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class Cart {

	private String idCart;
	private Map<Integer, DetailCart> productsInCart = new LinkedHashMap<Integer, DetailCart>();
	private Date creationDate;

	public Cart() {
		// TODO Auto-generated constructor stub
	}

	public Cart(String id) {
		this.idCart = id;
	}

	public String getIdCart() {
		return idCart;
	}

	public void setIdCart(String idCart) {
		this.idCart = idCart;
	}

	public Map<Integer, DetailCart> getProductsInCart() {
		return productsInCart;
	}

	public void setProductsInCart(Map<Integer, DetailCart> productsInCart) {
		this.productsInCart = productsInCart;
	}

}
