package model;

public class Client {
	private Integer idClient;
	private String login;
	private String password;
	private String firstName;
	private String lastName;
	private String clientTel;
	private String clientEmail;
	private String clientAdress;
	private String city;
	private int postalCode;
	private Cart clientCart;

	public Client() {
		clientCart = new Cart(this.login);
	}

	public Client(Integer idClient, String login, String firstName,
			String lastName, String password) {
		this.idClient = idClient;
		this.setLogin(login);
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
	}

	public Integer getIdClient() {
		return idClient;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getClientTel() {
		return clientTel;
	}

	public void setClientTel(String clientTel) {
		this.clientTel = clientTel;
	}

	public String getClientEmail() {
		return clientEmail;
	}

	public void setClientEmail(String clientEmail) {
		this.clientEmail = clientEmail;
	}

	public String getClientAdress() {
		return clientAdress;
	}

	public void setClientAdress(String clientAdress) {
		this.clientAdress = clientAdress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	public Cart getClientCart() {
		return clientCart;
	}

	public void setClientCart(Cart clientCart) {
		this.clientCart = clientCart;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

}
