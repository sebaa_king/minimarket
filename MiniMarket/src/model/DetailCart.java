package model;

public class DetailCart {

	private int product;
	private int quantity;
	
	public DetailCart() {

	}

	public DetailCart(int product,int quantity) {
		this.product=product;
		this.quantity=quantity;
	}

	public int getProduct() {
		return product;
	}

	public void setProduct(int product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	
}
