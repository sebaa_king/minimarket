<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Login</title>
<link rel="stylesheet" href="css/style_login.css">
<title>MiniMarket Login</title>
</head>
<body>
	<section class="container">
	<div class="login">
		<form method="Post" action='MarketController'>
			<p>
				<input type="text" name="login" value=""
					placeholder="Username or Email">
			</p>
			<p>
				<input type="password" name="password" value=""
					placeholder="Password">
			</p>
			<p class="remember_me"></p>
			<p class="submit">
				<input type="submit" name="commit" value="Login">
			</p>
		</form>
	</div>
	</section>
</body>
</html>