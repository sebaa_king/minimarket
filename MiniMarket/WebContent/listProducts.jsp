<%@page import="model.Product"%>
<%@page import="dao.DataAccessImp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<!-- Linking styles -->
<link rel="stylesheet" href="css/reset.css" type="text/css"
	media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css"
	media="screen">
<link rel="stylesheet" href="css/nivo-slider.css" type="text/css"
	media="screen">

<!-- Linking scripts -->
<script src="js/jquery.js"></script>
<script src="js/jquery.nivo.slider.pack.js"></script>
<script src="js/main.js"></script>

<!--[if lt IE 9]>
    <script type="text/javascript" src="js/html5.js"></script>
    <![endif]-->
</head>

<title>Product List</title>
</head>
<body>
	<div class="container">
		<%
		    DataAccessImp accessImp = new DataAccessImp();
		    Object objectIdUser = request.getSession().getAttribute("id_user");
		    int userId = -1;
		    String login = "";
		    if (objectIdUser != null) {			
				userId =(Integer)objectIdUser;
				login = (String) request.getSession().getAttribute("user");
		    }
		%>
		<header><!-- Defining the header section of the page --> <nav><!-- Defining the navigation menu -->
		<ul>
			<li><a href="index.jsp">Home</a></li>
			<li class="selected"><a href="listProducts.jsp">All Products</a></li>
			<li><a href="cart.jsp">My Cart</a></li>
			<li><a href="#">About</a></li>
		</ul>
		</nav>


		<div class="top_head">
			<!-- Defining the top head element -->
			<div class="logo">
				<!-- Defining the logo element -->
				<a href="#"> <img src="images/logo.jpg" title="E-Store template"
					alt="E-Store template" />
				</a>
			</div>

			<section id="search"><!-- Search form --> <%
     if (!login.isEmpty()) {
 %>
			<form action="#" onsubmit="return false;" method="get">
				<a href="MarketController?action=logout" title="logout"
					rel="external nofollow"><img alt="" src="images/logout.png"></a>
			</form>

			<ul id="social">
				<li><a href="#" title="username" rel="external nofollow"><img
						alt="" src="images/user.png"></a></li>
				<li><%=login%></li>
			</ul>
			<%
			    } else {
			%>
			<ul id="social">
				<li><a href="login.jsp" title="login" rel="external nofollow"><img
						alt="" src="images/login.png">Login</a></li>
			</ul>

			<%
			    }
			%> </section>
		</div>
		</header>
		<div id="slider">
			<!-- Defining the main content section -->

			<!-- Promo slider -->
			<section id="slider-wrapper">
			<div id="slider" class="nivoSlider">
				<img style="display: none;" src="images/promo1.jpg" alt=""
					title="#htmlcaption-1"> <img style="display: none;"
					src="images/promo2.jpg" alt="" title="#htmlcaption-2"> <img
					style="display: none;" src="images/promo3.jpg" alt=""
					title="#htmlcaption-3">
			</div>
			<div id="htmlcaption-1" class="nivo-html-caption">
				<h5 class="p2">Welcome to the our E-Shop</h5>
				<p>Put any description here</p>
			</div>
			<div id="htmlcaption-1" class="nivo-html-caption">
				<h5 class="p2">This is promo area</h5>
				<p>Put any description here</p>
			</div>
			<div id="htmlcaption-2" class="nivo-html-caption">
				<h5 class="p2">Where you can add any feature products</h5>
				<p>Put any description here</p>
			</div>
			<div id="htmlcaption-3" class="nivo-html-caption">
				<h5 class="p2">Or something else</h5>
				<p>Put any description here</p>
			</div>
			</section>
		</div>
		<div id="main">
			<!-- Defining submain content section -->
			<section id="content"><!-- Defining the content section #2 -->
			<div id="left">
				<h3>Available products</h3>
				<ul>
					<%
					    for (Product p : accessImp.listProduct()) {
					%>
					<li>
						<div class="img">
							<a href="#"><img alt="" src=<%=p.getImgProduct()%>></a>
						</div>
						<div class="info">
							<a class="title" href="#"><%=p.getProductName()%></a>

							<div class="price">
								<span class="st">Our price:</span><strong>$<%=p.getProductPrice()%></strong>
							</div>
							<div class="price">
								<span class="st">Quantity:</span><strong><%=(p.getProductQuantity() == 0) ? "OutOfStock" : p
			.getProductQuantity()%></strong>
							</div>
							<div class="actions">
								<a
									href="MarketController?action=addToCart&productId=<%=p.getProductId()%>&clientId=<%=userId%>">Add
									to cart</a>
							</div>
						</div>
					</li>
					<%
					    }
					%>
				</ul>
			</div>
			</section>
		</div>

	</div>

</body>
</html>